# Changelog

## v0.6
- Add unit tests for layers
- Enable Gitlab CI/CD for unit/integration test separately
- Enable testing for merge requests
- Remove duplicate tags due to commit_slug

## v0.5
- Add Plastimatch package to container
- Add parallel conversion of DICOM files to NRRD/NIFTI

## v0.4
- Add model zoo
- Switch testing infrastructure to pytest
- Add MNIST integration training/evaluation test
- Rename src directory to sidlp library name

## v0.3
- Add Gitlab CI/CD auto-build/test/deploy of Docker container
- Add basic data generator
- Add Keras model trainer and evaluator

## v0.2
- Move Python packages to requirements file and install in a separate Docker step
- Add medical image processing and deep learning Python packages

## v0.1
- Create Docker image based on Ubuntu 18.04 using CUDA 10.0 and CUDNN7
- Add base python packages
