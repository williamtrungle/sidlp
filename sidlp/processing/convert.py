import subprocess, nrrd
from pathlib import Path
from argparse import ArgumentParser
from dask import compute, delayed, diagnostics
from sidlp.utils import transformer, fmap, plot_gif

def parser():
    parser = ArgumentParser(description="Convert DICOM to NRRD")
    parser.add_argument("input")
    parser.add_argument("output")
    parser.add_argument("--artifact", "-a", help="set path to save gif of sample output")
    parser.add_argument("--dry-run", "-d", action="store_true")
    return parser

def main(input, output, artifact=None, dry_run=True):
    """Convert a DICOM directory dataset

    For each sample:
    The CT file is converted into a directory up (under sampleX). Each doseplan
    is renamed as dose-00X into a directory up. Each segmentation is converted
    into a directory named "contours" into a directory up.

    Input data directory layout:
    inputs/ > sample1/ | dose1/ | ct.dcm
            | sample2/ > dose2/ | doseplan.dcm
            | ...      | dose3/ > segmentation1.dcm
            |          | ...    | segmentation2.dcm
            |          |        | ...

    Output data directory layout:
    outputs/ | sample1/ | ct.nrrd       > eye.nrrd
             > sample2/ | dose-001.nrrd | nose.nrrd
             | ...      > contours/     | ears.nrrd
             |          | ...           | larynx.nrrd
             |          |               | ...
    """
    samples = Path(input).iterdir() # define inputs
    convert = plastimatch(Path(output), dry_run=dry_run) # define converter
    results = [convert(sample) for sample in samples] # define computation graph
    with diagnostics.ProgressBar():
        results = compute(results)
    if artifact and not dry_run:
        sample = fmap(fst)(results[0])[0]
        output_artifact(artifact, sample)

# ------------------------------------------------------------------------------

@transformer
def plastimatch(sample, output, to="nrrd", dry_run=True):
    """Converts DICOM to NRRD for one patient directory, returns nothing"""
    sample = Path(sample)
    path = Path(output)/sample.name
    if not dry_run:
        path.mkdir(parents=True, exist_ok=True)
    for i, plan in enumerate(sample.iterdir()):
        cmd = ["plastimatch", "convert",
               "--input", plan,
               "--output-dose-img", path/"dose{:03d}.{}".format(i,to)]
        if not i: # Only convert scan and mask once
            cmd += ["--output-img", path/"tensor.{}".format(to),
                    "--output-prefix", path/"contours",
                    "--prefix-format", to]
        yield run(fmap(str)(cmd), path, dry_run)

@delayed
def run(arguments, result=None, dry_run=True):
    if not dry_run:
        subprocess.run(arguments,
                       stdout=subprocess.DEVNULL,
                       stderr=subprocess.DEVNULL)
    return result

def output_artifacts(path, sample):
    path = Path(artifact)
    path.mkdir(exist_ok=True, parents=True)
    tensor = nrrd.read(str(sample/"ct.nrrd"))[0]
    plot_gif(tensor, (path/sample.name).with_suffix(".gif"))

# ------------------------------------------------------------------------------

if __name__ == "__main__":
    args = parser().parse_args()
    main(args.input, args.output, args.artifact, args.dry_run)
