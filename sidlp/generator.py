import numpy as np
from operator import itemgetter
from keras.utils import Sequence
from sidlp.utils import compose, fmap, swapattr, pure, lift1, extract, att
from sidlp.augmentations import ImageAugmentation

class DataGenerator(Sequence):
    """Convert a pair of numpy arrays to a keras Sequence

    Supports indexing, slicing, and iteration. Supports multi-inputs and
    multi-outputs: in that case, the inputs or outputs must be a list or
    a tuple of arrays, a single array is considered a single dataset input.
    """
    def __init__(self, inputs, outputs, batch_size=1):
        self.inputs, self.outputs = pure(inputs), pure(outputs)
        self.batch_size = batch_size

    def __call__(self, **kwargs):
        """Update attributes"""
        for k, v in kwargs.items():
            swapattr(self, k, v)
        return self

    def on_epoch_end(self):
        indices = np.random.permutation(lift1(len)(self.inputs))
        shuffle = fmap(itemgetter(indices))
        self.inputs, self.outputs = shuffle(self.inputs), shuffle(self.outputs)

    def __getitem__(self, idx):
        if isinstance(idx, slice):
            start = idx.start and idx.start * self.batch_size
            stop = idx.stop and idx.stop * self.batch_size
        else:
            start = idx * self.batch_size
            stop  = start + self.batch_size
        select = compose(extract, fmap(itemgetter((slice(start, stop), ...))))
        return select(self.inputs), select(self.outputs)

    def __iter__(self):
        self.index = 0
        return self

    def __next__(self):
        try:
            return self[self.index]
        except IndexError:
            raise StopIteration
        finally:
            self.index += 1

    def __len__(self):
        return lift1(len)(self.inputs)//self.batch_size

    @property
    def shape(self):
        return extract(fmap(itemgetter(slice(1, None)), att("shape"))(self.inputs))

    @property
    def num_classes(self):
        return extract(fmap(itemgetter(-1), att("shape"))(self.outputs))

# ------------------------------------------------------------------------------

class DataGenerator2D(DataGenerator):
    """Data generator with data augmentation support from Keras

    See keras.preprocessing.image.ImageDataGenerator for API. This class
    wraps featurewise_center and featurewise_std_normalization under a single
    key featurewise_standardization. Similarly for samplewise_standardization.
    This class also provides featurewise and samplewise normalization, which
    compresses the input's range between 0 and 1 (or selected values).
    """
    def __init__(self, *args, augments=None, **kwargs):
        super().__init__(*args, **wargs)
        self.augments = self.params = (augments or {}).copy()

    @property
    def augments(self):
        return self._augments

    @augments.setter
    def augments(self, d):
        self._augments = [ImageAugmentation(**d) for _ in self.inputs]

    def fit(self, x):
        return fmap(lambda a, x: a.fit(x))(self.augments, x)

    def feature_scale(self, x):
        x = lift(np.ndarray("float"))(x)
        x =  fmap(lambda a, x: fmap(a.standardize)(x))(self.augments, x)
        return x

    def random_transform(self, x):
        return fmap(lambda a, x: fmap(a.random_transform)(x))(self.augments, x)

    def __getitem__(self, idx):
        inputs, outputs = super().__getitem__(idx)
        inputs = self.random_transform(self.feature_scale(inputs))
        return inputs, outputs

# ------------------------------------------------------------------------------

class RadiotherapyGen(DataGenerator2D):
    """Data generator supporting scans and contour maps

    Expects for masks a single array or a list of arrays in the same order as
    for inputs and outputs. Masking operation is done first, before any random
    augmentation is applied.
    """
    def __init__(self, *args, masks=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.masks = masks if masks is None else pure(masks)

    def feature_scale(self, x):
        if x is not None:
            x = fmap(fmap(lambda x: np.multiply(*x)))(x, self.masks)
        return super().feature_scale(x)
