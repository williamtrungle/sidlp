import numpy as np
from keras.preprocessing.image import ImageDataGenerator
from sidlp.utils import (
    initialize, fmap, featurewise_normalization, samplewise_normalization
)

def only(*pred):
    """Checks whether only 1 of the predicates is set to True"""
    return sum(fmap(int, bool)(pred)) == 1

class ImageAugmentation(ImageDataGenerator):
    """Extending Keras's ImageDataGenerator

    Expects are rank-4 tensor of (samples, x, y, channels).

    Terminology:
    - featurewise: over the whole dataset
    - samplewise: per image
    - normalization: min=0, max=1 (or specified)
    - standardization: mean=0, std=1
    """
    @initialize
    def __init__(self, featurewise_normalization=False,
                       samplewise_normalization=False,
                       featurewise_standardization=False,
                       samplewise_standardization=False,
                       **kwargs):
        self._validate()
        self.min = None
        self.max = None
        if featurewise_standardization:
            kwargs["featurewise_center"] = True
            kwargs["featurewise_std_normalization"] = True
        if samplewise_standardization:
            kwargs["samplewise_center"] = True
            kwargs["samplewise_std_normalization"] = True
        super().__init__(**kwargs)

    def _validate(self):
        """
        The main provided feature is featurewise/samplewise feature scaling between
        0 and 1 (or user-defined), which is named "normalization", and consolidating
        "center" and "std_normalization" under "standardization".
        """
        if not only(self.featurewise_normalization,
                    self.samplewise_normalization,
                    self.featurewise_standardization,
                    self.samplewise_standardization):
            raise ValueError("ImageAugmentation specifies more"
                             "than one feature-scaling option")
        for deprecated in ("featurewise_center", "featurewise_std_normalization",
                           "samplewise_center", "samplewise_std_normalization"):
            if self.kwargs.get(deprecated, False):
                raise ValueError("Option %s is deprecated" % deprecated)

    def fit(self, x, augments=False, rounds=1, seed=None):
        super().fit(x, augments, rounds, seed)
        if self.featurewise_normalization:
            self.min = np.min(x, axis=(0, self.row_axis, self.col_axis))
            self.max = np.max(x, axis=(0, self.row_axis, self.col_axis))
        return self.min, self.max, self.mean, self.std, self.principal_components

    def standardize(self, x):
        x = super().standardize(x)
        if self.featurewise_normalization:
            x = featurewise_normalization(self.min, self.max,
                    amax=self.featurewise_normalization)(x)
        if self.samplewise_normalization:
            x = samplewise_normalization(amax=self.featurewise_normalization)(x)
        return x
