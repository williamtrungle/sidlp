"""Utilities function dealing with network shapes and architectures"""
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from array2gif import write_gif
# from functional import transformer, nmap, apply

sns.set()

def conv_padding_inverse(kernel, strides, padding=0, dilation=1):
    """Returns the shape loss due to integer rounding from a convolution.
    Useful to invert and preserve the shape un Unet architecture. Use this
    value as the output_padding parameter in Conv2DTranspose."""
    return lambda shape: (shape + 2*padding - kernel - (kernel-1)*(dilation-1)) % strides

# ------------------------------------------------------------------------------

def featurewise_standardization(mean=None, std=None, **_):
    def _featurewise_standardization(x):
        m = x.mean() if mean is None else mean
        center = x - m
        s = center.std() if std is None else std
        return center/s
    return _featurewise_standardization

def samplewise_standardization(**_):
    return lambda x: nmap(featurewise_standardization())(x)

def featurewise_normalization(min=None, max=None, amin=None, amax=None, **_):
    def _featurewise_normalization(x):
        # If amax is not set, max = amin  and min = 0
        max = float(amax or amin or 1)
        min = float((amin or 0) if amax else 0)
        return amax*(x-x.min()+min)/(x-x.min()+min).max()
    return _featurewise_normalization

def samplewise_normalization(amin=None, amax=None, **_):
    return lambda x: nmap(featurewise_normalization(amin, amax))(x)

# ------------------------------------------------------------------------------

def plot_fig(*images, index=None, cmap=None, vmin=None, vmax=None):
    with sns.axes_style("dark"):
        fig, *axes = plt.subplots(1, len(images), figsize=(20, 5))
        axes = np.array(axes[0]) if isinstance(axes[0], np.ndarray) else axes
        if index is None:
            index = np.random.randint(len(images[0]))
        for ax, img in zip(axes, images):
            pos = ax.imshow(np.squeeze(img[index]), cmap, vmin=vmin, vmax=vmax)
            fig.colorbar(pos, ax=ax)
        return fig

def plot_gif(tensor, output):
    write_gif(apply_(tensor)(transformer(np.ndarray.astype("uint8"),
                             np.squeeze,
                             transformer(np.expand_dims)(axis=0),
                             transformer(np.moveaxis)(-1, 0),
                             transformer(np.repeat)(3, axis=1),
                             transformer(featurewise_normalization)(255)),
              str(output),
              fps=30))
