"""Functional programming utilities"""
import numpy, functools, operator

def transformer(func):
    """Turns a function f(a,b, ...) into a function f(b, ...)(a)

    Note that the resulting function is not closed under η-conversion,
    since the object returned may have side-effects (ie. favor explicitly
    defining arguments)
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        return lambda x: func(x, *args, **kwargs)
    return wrapper

def lazy(func):
    """Delays the creation of tensor nodes by wrapping in a lambda, preventing
    bugs where the same node is reused during partial applications of params.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        return lambda x: func(*args, **kwargs)(x)
    return wrapper

# ------------------------------------------------------------------------------

def unzip(itr):
    return list(zip(*itr))

def compose(*fs):
    compose2 = lambda f, g: lambda *a, **kw: f(g(*a, **kw))
    return functools.reduce(compose2, fs, lambda x:x)

def compose_(*fs):
    return compose(*list(reversed(fs)))

def apply(*args, **kwargs):
    def wrapper(*fs):
        return compose(*fs)(*args, **kwargs)
    return wrapper

def apply_(*args, **kwargs):
    return lambda *fs: compose_(*fs)(*args, **kwargs)

def tmap(*f):
    def wrapper(*itr):
        return tuple(map(compose(*f), *itr))
    return wrapper

def fmap(*f):
    def wrapper(*itr):
        return list(map(compose(*f), *itr))
    return wrapper

def nmap(*f):
    def wrapper(*itr):
        return numpy.array(list(map(compose(*f), *itr)))
    return wrapper

def fst(x):
    return x[0]

def snd(x):
    return x[1]

def first(f):
    def wrapper(x):
        return (f(fst(x)), snd(x))
    return wrapper

def second(f):
    def wrapper(x):
        return (fst(x), f(snd(x)))
    return wrapper

def cross(f, g):
    def wrapper(x):
        return (f(x), g(x))
    return wrapper

def curry(f):
    def wrapper(*args, **kwargs):
        return f(*args, **kwargs)
    return wrapper

def foreach(*fs):
    f = compose(*fs)
    def wrapper(*itr):
        for it in zip(*itr):
            f(*it)
    return wrapper

def zipwith(fs, *itr):
    return fmap(lambda x: x[0](*x[1:]))(zip(fs, *itr))

def mtd(name, *args, **kwargs):
    def wrapper(x):
        return getattr(x, name)(*args, **kwargs)
    return wrapper

def att(name):
    def wrapper(x):
        return getattr(x, name)
    return wrapper

def const(f):
    @functools.wraps(f)
    def wrapper(_):
        return lambda x: f(x)
    return wrapper

def extract(x):
    if len(x) == 1:
        return x[0]
    else:
        return x

def pure(x):
    return list(x) if isinstance(x, (list, tuple)) else [x]

# Returns the lifted operation on the first element only
# (or the only element if x is not a list or a tuple)
# In otherwords:
#
# f(x) or map(f, x) ->
# convert to list if necessary ->
# take the first value
lift1 = lambda *fs: compose(operator.itemgetter(0), pure, fmap(*fs))
