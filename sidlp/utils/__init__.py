import numpy as np
from .functional import *
from .class_init import initialize
from .network import (
    featurewise_standardization, featurewise_normalization,
    samplewise_standardization, samplewise_normalization,
    plot_fig, plot_gif, conv_padding_inverse
)

def swapattr(d, k, v):
    if hasattr(d, k):
        setattr(d, k, v)
    else:
        raise KeyError(k)
