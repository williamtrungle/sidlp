from collections import defaultdict
from sidlp.utils.functional import transformer
from keras.layers import *
import warnings

initializations = defaultdict(lambda _: "glorot_uniform",
        sigmoid="glorot_uniform",
        softmax="glorot_uniform",
        relu="he_normal",
        selu="lecun_normal")

class LayerInitializationWarning(UserWarning): pass

def validate_initializations(activation, kernel_initializer):
    if initializations[activation] != kernel_initializer:
        warnings.warn(
            "Using %s with %s instead of the recommended %s" % \
            (activation, kernel_initializer, initializations[activation]),
            LayerInitializationWarning)

@transformer
def batchdrop(layer, batch_norm=False, dropout=False):
    if batch_norm:
        layer = BatchNormalization()(layer)
    if dropout:
        layer = Dropout(dropout)(layer)
    return layer

@transformer
def conv2dl(layer,
           filters,
           kernel_size=3,
           strides=1,
           activation="relu",
           kernel_initializer="he_normal",
           kernel_regularizer=None,
           padding="valid",
           batch_norm=False,
           dropout=False,
           **kwargs):
    params = dict(filters=filters,
                  kernel_size=kernel_size,
                  strides=strides,
                  activation=activation,
                  padding=padding,
                  kernel_initializer=kernel_initializer,
                  kernel_regularizer=kernel_regularizer)
    if kwargs.get("input_shape", None):
        params["input_shape"] = kwargs["input_shape"]
    layer = Conv2D(**params)(layer)
    layer = batchdrop(batch_norm, dropout)(layer)
    return layer

@transformer
def dconv2dl(layer,
            filters,
            kernel_size=3,
            strides=1,
            activation="relu",
            output_padding=None,
            kernel_initializer="he_normal",
            kernel_regularizer=None,
            padding="valid",
            batch_norm=False,
            dropout=False,
            **kwargs):
    params = dict(filters=filters,
                  kernel_size=kernel_size,
                  strides=strides,
                  activation=activation,
                  output_padding=output_padding,
                  padding=padding,
                  kernel_initializer=kernel_initializer,
                  kernel_regularizer=kernel_regularizer)
    if kwargs.get("input_shape", None):
        params["input_shape"] = kwargs["input_shape"]
    layer = Conv2DTranspose(**params)(layer)
    layer = batchdrop(batch_norm, dropout)(layer)
    return layer

@transformer
def densel(layer,
          units,
          activation="relu",
          kernel_initializer="he_normal",
          kernel_regularizer=None,
          batch_norm=False,
          dropout=False,
          **kwargs):
    params = dict(units=units,
                  use_bias=not batch_norm,
                  activation=activation,
                  kernel_initializer=kernel_initializer,
                  kernel_regularizer=kernel_regularizer)
    if kwargs.get("input_shape", None):
        params["input_shape"] = kwargs["input_shape"]
    layer = Dense(**params)(layer)
    layer = batchdrop(batch_norm, dropout)(layer)
    return layer
