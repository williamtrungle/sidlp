from sidlp.utils.functional import transformer, lazy, compose, tmap
from sidlp.utils.network import conv_padding_inverse as pad
from sidlp.models.layers import *
from keras.layers import *
from keras.models import *
import keras.backend as K

@transformer
def dense(layer,
          *units,
          activation="relu",
          final_activation=None,
          kernel_initializer="he_normal",
          final_initializer=None,
          kernel_regularizer=None,
          batch_norm=False,
          dropout=False,
          input_shape=None,
          **_):
    *units, final = units
    final_activation = final_activation or activation
    final_initializer = final_initializer or initializations[final_activation]
    validate_initializations(activation, kernel_initializer)
    validate_initializations(final_activation, final_initializer)
    params = dict(activation=activation,
                  kernel_initializer=kernel_initializer,
                  kernel_regularizer=kernel_regularizer,
                  batch_norm=batch_norm,
                  dropout=dropout,
                  input_shape=input_shape)
    for unit in units:
        layer = densel(unit, **params)(layer)
        params.pop("input_shape", None)
    layer = densel(final, **dict(params,
        activation=final_activation,
        kernel_initializer=final_initializer))(layer)
    return layer

@transformer
def conv2d(layer,
          *filters,
          kernel_size=3,
          strides=1,
          activation="relu",
          final_activation=None,
          kernel_initializer="he_normal",
          final_initializer=None,
          padding="valid",
          kernel_regularizer=None,
          batch_norm=False,
          dropout=False,
          input_shape=None,
          **_):
    *filters, final = filters
    final_activation = final_activation or activation
    final_initializer = final_initializer or initializations[final_activation]
    validate_initializations(activation, kernel_initializer)
    validate_initializations(final_activation, final_initializer)
    params = dict(activation=activation,
                  kernel_initializer=kernel_initializer,
                  kernel_regularizer=kernel_regularizer,
                  kernel_size=kernel_size,
                  strides=strides,
                  padding=padding,
                  batch_norm=batch_norm,
                  dropout=dropout,
                  input_shape=input_shape)
    for filter in filters:
        if isinstance(filter, int):
            layer = conv2dl(filter, **params)(layer)
        else:
            for ft in filter:
                layer = conv2dl(ft, **params)(layer)
        layer = MaxPooling2D()(layer)
        params.pop("input_shape", None)
    if not(final_activation and final_initializer):
        activation = final_activation or activation
        kernel_initializer = final_initializer or initializations[activation]
        layer = conv2dl(final, **dict(params,
            activation=final_activation,
            kernel_initializer=kernel_initializer))(layer)
    else:
        activation = final_activation or activation
        kernel_initializer = final_initializer or initializations[activation]
        layer = conv2dl(final, **dict(params,
            activation=final_activation,
            kernel_initializer=kernel_initializer))(layer)
        layer = MaxPooling2D()(layer)
    return layer


# ------------------------------------------------------------------------------

@transformer
def unet_p(layer,
           filters,
           pool=2,
           merge=None,
           bottleneck=None,
           **params):
    if filters:
        filter, *filters = filters
        conv = conv2dl(filter, **dict(params, padding="same", strides=1))
        input = conv(layer)
        output = compose(
                conv,
                lazy(UpSampling2D)(pool),
                unet_p(filters, pool, merge, bottleneck, **params),
                lazy(MaxPooling2D)(pool))(input)
        layer = merge([input, output]) if merge else output
    elif bottleneck:
        layer = bottleneck(layer)
    return layer

@transformer
def unet_s(layer,
           filters,
           kernel_size=3,
           strides=2,
           merge=None,
           bottleneck=None,
           **params):
    assert kernel_size >= strides, "Want: %s >= %s" % (kernel_size, strides)
    if filters:
        filter, *filters = filters
        padding = tmap(pad(kernel_size, strides))(K.int_shape(layer)[1:3])
        output = compose(
                dconv2dl(filter, kernel_size, strides, **params, output_padding=padding),
                unet_s(filters, kernel_size, strides, merge, bottleneck, **params),
                conv2dl(filter, kernel_size, strides, **params))(layer)
        layer = merge([layer, output]) if merge else output
    elif bottleneck:
        layer = bottleneck(layer)
    return layer

@transformer
def unet(layer,
         filters,
         kernel_size=3,
         strides=None,
         pool=None,
         merge=None,
         bottleneck=None,
         **params):
    assert bool(strides) != bool(pool), "Please specify only one of strides or pool"
    if strides:
        return unet_s(filters, kernel_size, strides, merge, bottleneck, **params)(layer)
    if pool:
        return unet_p(filters, pool, merge, bottleneck, **params, kernel_size=kernel_size)(layer)
