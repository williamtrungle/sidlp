import pytest, re, os
from sidlp.models.layers import batchdrop, conv2dl, dconv2dl, densel
from keras.models import Model
from keras.layers import Input, Dense, Conv2D, Conv2DTranspose, BatchNormalization, Dropout

os.environ["CUDA_VISIBLE_DEVICES"] = ""

def name(s):
    return re.sub(r"_\d+$", "", s.name)

@pytest.fixture
def inputs():
    return Input((512, 512, 3))

class TestBatchDrop:
    def test_id(self, inputs):
        outputs = batchdrop()(inputs)
        model = Model(inputs, outputs)
        assert len(model.layers) == 1
        assert name(model.layers[-1]) == "input"
        assert model.layers[-1].output_shape == (None, 512, 512, 3)

    def test_batch(self, inputs):
        model = Model(inputs, batchdrop(batch_norm=True)(inputs))
        assert name(model.layers[-1]) == "batch_normalization"

    def test_drop(self, inputs):
        model = Model(inputs, batchdrop(dropout=0.42)(inputs))
        assert name(model.layers[-1]) == "dropout"
        assert model.layers[-1].rate == 0.42

    def test_batchdrop(self, inputs):
        model = Model(inputs, batchdrop(batch_norm=True, dropout=0.71)(inputs))
        for layer, n in zip(model.layers, ("input", "batch_normalization", "dropout")):
            assert name(layer) == n
            assert layer.output_shape == (None, 512, 512, 3)
        assert layer.rate == 0.71

class TestConv2D:
    def test_3_1_valid(self, inputs):
        model = Model(inputs, conv2dl(32)(inputs))
        assert name(model.layers[-1]) == "conv2d"
        assert model.output_shape == (None, 510, 510, 32)

    def test_5_2_valid(self, inputs):
        model = Model(inputs, conv2dl(64, kernel_size=5, strides=2)(inputs))
        assert model.output_shape == (None, 254, 254, 64)

    def test_4_1_same(self, inputs):
        model = Model(inputs, conv2dl(128, kernel_size=4, padding="same")(inputs))
        assert model.output_shape == (None, 512, 512, 128)

    def test_batch(self, inputs):
        model = Model(inputs, conv2dl(10, kernel_size=1, padding="same", batch_norm=True)(inputs))
        assert name(model.layers[-1]) == "batch_normalization"
        assert model.output_shape == (None, 512, 512, 10)

    def test_drop(self, inputs):
        model = Model(inputs, conv2dl(20, kernel_size=1, padding="same", dropout=0.17)(inputs))
        assert name(model.layers[-1]) == "dropout"
        assert model.output_shape == (None, 512, 512, 20)

    def test_shape(self, inputs):
        model = Model(inputs, conv2dl(3, kernel_size=1, padding="same", batch_norm=True, dropout=0.17)(inputs))
        for layer in model.layers:
            assert layer.output_shape == (None, 512, 512, 3)
