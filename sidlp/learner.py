import logging; logging.getLogger('tensorflow').setLevel(logging.FATAL)

import csv, pathlib
import numpy as np
from keras.callbacks import CSVLogger, ModelCheckpoint, Callback
from keras.optimizers import Adam
from keras.models import load_model

class Learner:
    """Keras model trainer and evaluator with sensible defaults

    Adds auto checkpointing of best and latest weights, logging, early
    termination if monitor reaches 100%, resume training, and auto evaluating
    model performance after training completes or is interrupted. Inherit from
    this class to extend custom functionalities.

    Data expected to be a generator implementing keras.utils.Sequence
    """
    def __init__(self, model, loss, train_generator,
                       valid_generator=None,
                       metrics=None,
                       weighted_metrics=None,
                       class_weight=None,
                       optimizer=Adam(3e-4),
                       monitor="loss",
                       epochs=1,
                       augmentations=1,
                       callbacks=None,
                       save_weights_only=True,
                       output_dir=None,
                       history_log="training.log",
                       checkpoint="checkpoint.h5",
                       resume=True,
                       verbose=1):
        self._model = model
        self.loss = loss
        self.train_generator = train_generator
        self.valid_generator = valid_generator
        self.metrics = coerce_list(metrics)
        self.weighted_metrics = coerce_list(weighted_metrics)
        self.class_weight = class_weight
        self.optimizer = optimizer
        self._monitor = monitor.replace("val_", "")
        self.epochs = epochs
        self.augmentations = augmentations
        self._callbacks = list(callbacks or [])
        self.save_weights_only = save_weights_only
        self.resume = resume
        self.verbose = verbose
        self.output_dir = output_dir and pathlib.Path(output_dir)
        if self.output_dir:
            self.output_dir.mkdir(parents=True, exist_ok=True)
            self.history_log = self.output_dir/history_log
            self.checkpoint  = self.output_dir/checkpoint
        else:
            self.history_log = self.checkpoint = None
        self.trained = False
        self.compile()

    def compile(self):
        self._model.compile(self.optimizer,
                            loss=self.loss,
                            metrics=self.metrics,
                            weighted_metrics=self.weighted_metrics)

    def fit(self):
        try:
            return self.model.fit_generator(self.train_generator,
                    steps_per_epoch=len(self.train_generator)*self.augmentations,
                    validation_data=self.valid_generator,
                    epochs=self.epochs,
                    initial_epoch=self.initial_epoch,
                    class_weight=self.class_weight,
                    callbacks=self.callbacks,
                    verbose=self.verbose and (2 if self.verbose == 1 else 1))
        except KeyboardInterrupt:
            print()
        finally:
            self.trained = True

    def evaluate(self, data=None):
        return dict(zip(self.model.metrics_names,
                        self.model.evaluate_generator(data or self.valid_generator,
                                                      verbose=self.verbose==2)))

    def predict(self, data=None):
        return self.model.predict_generator(data or self.valid_generator,
                                            verbose=self.verbose==2)

    @property
    def model(self):
        """The trained model if fit was called, otherwise the latest model"""
        if self.trained and self.output_dir:
            if self.saved_weights_only:
                self._model.load_weights(str(self.best_model))
            else:
                self._model = load_model(str(self.best_model))
        return self._model

    @property
    def callbacks(self):
        """Default provided callbacks

        - CSVLogger: <output_dir>/training.log
        - ModelCheckpoint (latest): <output_dir>/checkpoint.h5
        - ModelCheckpoint (best <monitor>): <output_dir>/<monitor>.h5
        - TerminateOnBaseline (first metric in metrics)
        """
        callbacks = self._callbacks
        if self.metrics:
            callbacks += [TerminateOnBaseline(self.metrics[0])]
        if self.output_dir:
            callbacks += [
                    CSVLogger(str(self.history_log), append=self.resume),
                    ModelCheckpoint(str(self.checkpoint),
                        save_weights_only=self.save_weights_only),
                    ModelCheckpoint(str((self.output_dir/self._monitor)+".h5"),
                        save_weights_only=self.save_weights_only,
                        save_best_only=True,
                        monitor=self.monitor,
                        mode="min" if "loss" in self.monitor else "max")]
        return callbacks

    @property
    def monitor(self):
        """The validation set monitored metric for model checkpointing"""
        monitors = (self.loss, *self.metrics)
        if self._monitor not in monitors:
            raise ValuError("%s not in %s" % (self._monitor, monitors))
        return "val_" + self._monitor

    @property
    def initial_epoch(self):
        """The initial epoch for training resuming based on the csv log file"""
        try:
            with self.history_log.open() as history:
                for row in csv.reader(history):
                    pass
                return int(row[0])
        except (StopIteration, FileNotFoundError, AttributeError):
            return 0

    def __repr__(self):
        batch = self.train_generator.batch_size
        length = len(self.train_generator) * batch
        agm = self.augmentations
        shape = (length,) + self.train_generator.shape
        return "\n".join([
            "Train: %s x (batch=%s) x (augmentations=%s) = %s samples" % \
                    (shape, batch, agm, length*agm),
            "Valid: %s samples" % len(self.valid_generator)])

# ------------------------------------------------------------------------------

def coerce_list(value):
    if value is None:
        return []
    elif isinstance(value, (list, tuple, np.ndarray)):
        return list(value)
    else:
        return [value]

class TerminateOnBaseline(Callback):
    """Callback that terminates training when either acc or val_acc reaches
    a specified baseline"""
    def __init__(self, monitor='acc', baseline=1.0):
        super(TerminateOnBaseline, self).__init__()
        self.monitor = monitor
        self.baseline = baseline

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        acc = logs.get(self.monitor)
        if acc is not None:
            if acc >= self.baseline:
                print('Epoch %d: Reached baseline, terminating training' % (epoch))
                self.model.stop_training = True
