# Sructure and Interpretation of a Deep Learning Project

This repository is my personal template for a typical deep learning project (specialized in medical imaging) that uses DVC for data version control, and Docker along with a CI/CD tool for environment reproducibility (either Bitbucket+DockerHub or Gitlab). For rapid development, direnv is used to generate a Python3 virtual environment.

My preferred pipeline uses Plastimatch to convert DICOM to NRRD, and HDF5 as a data storage format, and Keras for modeling. Preprocessing parallelization is achieved using Dask. Results are tracked via the remote repository's wiki (also a repository) since branches in the main repository are already used to track experiments. CometML serves as a dashboard to monitor experiments in progress.

## Project Structure

A typical machine learning project contains 3 components: a model to be trained, a dataset, and a training schedule. As such this translates into the 3 core module in SIDLP.

### Learner

The Learner is the bread and butter of this repository and contains a boilerplate-reducing class that accepts all the usual hyperparameters along with sane defaults such as auto-checkpointing and early termination in case of overfitting and takes care of training and validating on the training and validation sets (and the test set if available).

One constraint is that the data be made available through a data generator, which simplifies shuffling and batching processes.

### Generator

The data generator is fairly self-explanatory and allows returning batches of data during training. One advantage of this model is out-of-core loading of data, which allows us to bypass system memory limits. Another advantage is that this enables online data augmentation schemes on a per batch basis. Data is expected as 3 pre-split Numpy-array like datasets for train/valid/test.

### Model

The model zoo contains a small collection of useful models we use as starting points for many research projects:
- Classification CNN (Conv -> Dense)
- UNet (Conv -> ConvTranspose with Add/Concatenate skip connection)

## Preprocessing
Raw inputs sometimes arrive in DICOM format which requires converting into a more usable format such as NRRD. Plastimatch is favored over Pydicom due to native support for converting RTStruct (segmentation masks) and dose plans.

## Reporting
Todo
