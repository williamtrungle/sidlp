import pytest, os
import numpy as np

from sidlp.models           import dense, conv2d
from sidlp.learner          import Learner
from sidlp.generator        import DataGenerator
from sidlp.utils            import unzip, zipwith, fmap

from keras                  import datasets
from keras.utils            import to_categorical
from keras.models           import Model
from keras.layers           import Input, Flatten

os.environ["CUDA_VISIBLE_DEVICES"] = ""

@pytest.fixture
def mnist():
    # (n, 28, 28) -> (n, 28, 28, 1)
    process_x = lambda x: np.expand_dims(x, axis=-1).astype("float")/255
    # (n,) -> (n, 10)
    process_y = lambda y: to_categorical(y, y.max()+1)
    # Load Data
    train, valid = map(DataGenerator,
                       *zipwith(fmap(fmap)([process_x, process_y]),
                                unzip(datasets.mnist.load_data())))
    return dict(train=train, valid=valid)

def test_mnist(mnist):
    # Define model
    inputs = Input(mnist["train"].shape)
    cnn = conv2d(32, 64)(inputs)
    flat = Flatten()(cnn)
    fnn = dense(128, mnist["train"].num_classes, final_activation="softmax")(flat)
    model = Model(inputs, fnn)
    assert model.count_params() == 225034
    learner = Learner(model,
                      "categorical_crossentropy",
                      mnist["train"](batch_size=32),
                      mnist["valid"],
                      metrics="acc",
                      verbose=1)
    history = learner.fit()
    metrics = learner.evaluate()
    assert metrics["loss"] < 0.1
    assert metrics["acc"] > 0.95
